<!--Please answer the below questions to the best of your ability.-->

#### What's this issue all about? (Background and context)

#### What are the overarching goals for the research?

#### What hypotheses and/or assumptions do you have?

####  What research questions are you trying to answer?

#### What persona, persona segment, or customer type experiences the problem most acutely?

#### What business decisions will be made based on this information?

#### What, if any, relevant prior research already exists?

<!-- Have a look at our UXR_Insights repo: https://gitlab.com/gitlab-org/uxr_insights and look at our sales calls archive: Chorus.ai (requires getting access) -->

#### What timescales do you have in mind for the research?

#### Who will be leading the research?

#### Relevant links (opportunity canvas, discussion guide, notes, etc.)

<!-- Assign this issue to the researcher for your group. If you're unsure which researcher is assigned to your group, you can check:
https://about.gitlab.com/handbook/product/categories/

If there is no one listed or you're unsure, assign the issue to the UX Research Director. Leaving an issue unassigned means it may go unnoticed by the UX Research Team.
-->

<!-- #### TODO Checklist
 Consider adding a checklist in order to keep track of what stage the research is up to. Some possible checklist templates are here:
 https://about.gitlab.com/handbook/engineering/ux/ux-research-training/templates-resources-for-research-studies/#checklists
 -->

/label ~"workflow::problem validation"
/label ~"UX Problem Validation"
/label ~"UX Research Backlog"
/confidential 
