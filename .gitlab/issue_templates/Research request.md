<!-- Please do not use this template if you are a: Product Manager, UX Designer or UX Researcher. Instead, please use either the `Problem validation` or `Solution validation` template`. -->

#### What’s this issue all about?

#### What hypotheses and/or assumptions do you have?

#### What questions are you trying to answer?

##### Core questions 

<!-- What needs to be answered to move work forward? -->

##### Additional questions

<!-- Is there anything else you'd like to know? -->

#### What persona, persona segment, or customer type experiences the problem most acutely?

#### What business decisions will be made based on this information?

<!-- Assign this issue to the Product Manager, Product Designer and UX Researcher for the relevant group. If you're unsure of who that is, you can check here:
https://about.gitlab.com/handbook/product/categories/

If there is no one listed or you're unsure, assign the issue to the UX Research Director. Leaving an issue unassigned means it may go unnoticed by the UX Research Team.
-->

/label ~"UX Research Backlog"

/label ~"UX Research request"